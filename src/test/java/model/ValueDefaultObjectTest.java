package model;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ValueDefaultObjectTest {

    private static ValueDefaultObject instance;

    @BeforeClass
    public static void setUp() {
        instance = new ValueDefaultObject();
    }

    @Test
    public void getValue_IsEmptyValue_test() {
        Assert.assertTrue(instance.getValue().isEmpty());
    }

}