package readers;

import exceptions.ConfigurationException;
import model.ConfigurationData;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

public class PropertyFileReaderTest {

    private static PropertyFileReader instance;

    @BeforeClass
    public static void setUp() {
        instance = new PropertyFileReader("files/test.properties");
    }

    @Test(expected = ConfigurationException.class)
    public void save_whenKeyIsNull_thenReturnConfigurationException() {
        instance.save(new ConfigurationData(null, null));
    }

    @Test(expected = ConfigurationException.class)
    public void save_whenKeyIsEmpty_thenReturnConfigurationException() {
        instance.save(new ConfigurationData("", null));
    }

    @Test
    public void readValue_test() {
        ConfigurationData data = instance.readValue("test.key");
        assertNotNull(data);
        assertFalse(data.getData().isEmpty());
    }

    @Test
    public void sizeIsOneTest() {
        assertEquals(instance.size().intValue(), 1);
    }

    @Test
    public void remove_test() {
        instance.remove("test.key");
        assertEquals(instance.size().intValue(), 0);
    }

    @Test
    public void errorFileNotFound_test() {
        try {
            new PropertyFileReader("files/no-exists.properties");
            fail();
        } catch (ConfigurationException ce) {
            assertEquals(ce.getMessage(), "files/no-exists.properties: file not found");
        }
    }

}