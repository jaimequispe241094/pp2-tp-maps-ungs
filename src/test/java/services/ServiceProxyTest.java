package services;

import builder.LocationSearchBuilder;
import factory.MockLocationFactory;
import model.LocationProxy;
import static org.junit.Assert.*;
import model.LocationSearchDto;
import org.junit.BeforeClass;
import org.junit.Test;

public class ServiceProxyTest {

    private static LocationProxy locationProxy;
    private static LocationSearchDto searchDto;

    @BeforeClass
    public static void setUp() {
        locationProxy = new ServiceProxy(new MockLocationFactory());
    }

    @Test
    public void getRoad_caseListSizeOneTest() {
        searchDto = LocationSearchBuilder.buildMock(22, 66, 22, 66);
        assertEquals(locationProxy.getRoad(searchDto).size(), 1);
    }

    @Test
    public void getRoad_caseListSizeThreeTest() {
        searchDto = LocationSearchBuilder.buildMock(11, 22, 33, 44);
        assertEquals(locationProxy.getRoad(searchDto).size(), 3);
    }

    @Test
    public void getRoad_caseListSizeEmptyTest() {
        searchDto = LocationSearchBuilder.buildMock(23, 41, 40, 60);
        assertTrue(locationProxy.getRoad(searchDto).isEmpty());
    }

}