package myCache;

import builder.LocationSearchBuilder;
import implementation.ApiLocationMock;
import model.DataCache;
import static org.junit.Assert.*;
import model.LocationSearchDto;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

public class LocationCacheTest {

    private static LocationCache instanceWithDelegation, instanceWithOutDelegation;
    private static DataCache dataCache;
    private static LocationSearchDto dtoSearch;

    @BeforeClass
    public static void setUp() {
        dataCache = new LocationData();

        instanceWithDelegation = new LocationCache(new ApiLocationMock(), dataCache);
        instanceWithOutDelegation = new LocationCache(null, dataCache);

        dtoSearch = LocationSearchBuilder.buildMock(22, 66, 22, 66);
    }

    @After
    public void clean() {
        dataCache.delete(dtoSearch);
    }

    @Test
    public void isActive() {
        assertTrue(instanceWithDelegation.isAvailable());
    }

    @Test
    public void isOn_whenOn_isActive() {
        instanceWithDelegation.on();
        assertTrue(instanceWithDelegation.isAvailable());
    }

    @Test
    public void isOff_whenOff_isActive() {
        instanceWithDelegation.off();
        assertTrue(instanceWithDelegation.isAvailable());
    }

    @Test
    public void getData_whenDoNotHaveNextDelegation_thenReturnEmptyList() {
        assertTrue(instanceWithOutDelegation.getData(dtoSearch).isEmpty());
    }

    @Test
    public void getData_whenDoHaveNextDelegation_thenListWithSizeOne() {
        assertEquals(instanceWithDelegation.getData(dtoSearch).size(), 1);
    }

}