package userstories;

import exceptions.InvalidReadOrWriteFileException;
import model.ConfigurationData;
import static org.junit.Assert.*;
import model.ConfigurationReader;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import readers.MockConfigurationReader;

public class UserCase04 {

    private static ConfigurationReader A, B, C, D;
    private static ConfigurationData data, dataError, dataOne, dataTwo;

    @BeforeClass
    public static void setUp() {
        A = new MockConfigurationReader();
        B = new MockConfigurationReader();
        C = new MockConfigurationReader();
        D = new MockConfigurationReader();

        data = new ConfigurationData("test.insert.key", "123");
        dataError = new ConfigurationData("FILE.OPEN.KEY", "123");
        dataOne = new ConfigurationData("key.path", "document/files");
        dataTwo = new ConfigurationData("key.exists", "3");

        B.save(dataOne);
        B.save(dataTwo);
    }

    @After
    public void clean() {
        A.remove("test.insert.key");
        B.remove("test.insert.key");
        C.remove("test.insert.key");
    }

    @Test
    public void readValueTest_whenKeyNotExists_thenReturnEmptyValue() {
        assertEquals(A.readValue("NO.EXISTS").getData(), StringUtils.EMPTY);
    }

    @Test
    public void readValueTest_whenFileNotExists_thenReturnEmptyValue() {
        assertEquals(C.readValue("NO.EXISTS").getData(), StringUtils.EMPTY);
    }

    @Test
    public void readValueTest_whenKeyExists_thenReturnAssociateValue() {
        assertEquals(B.readValue("key.exists").getData(), "3");
        assertEquals(B.readValue("key.path").getData(), "document/files");
    }

    @Test
    public void size_test() {
        assertEquals(A.size().intValue(), 0);
        assertEquals(B.size().intValue(), 2);
    }

    @Test
    public void saveTest_whenSavedSameKey_thenValueIsUpdated() {
        dataOne.setData("~/Downloads");
        assertTrue(B.save(dataOne));
        assertEquals(B.size().intValue(), 2);
        assertEquals(B.readValue("key.path").getData(), "~/Downloads");
    }

    @Test
    public void saveTest_whenSavedIsOk_thenReturnTrue() {
        assertTrue(A.save(data));
        assertTrue(B.save(data));
        assertTrue(C.save(data));
    }

    @Test
    public void saveTest_whenSavedIsNotOk_thenReturnFalse() {
        try {
            D.save(dataError);
            fail();
        } catch (InvalidReadOrWriteFileException e) {
            assertEquals(e.getMessage(), "Error - File is open.");
        }
    }

}