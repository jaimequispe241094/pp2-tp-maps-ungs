package userstories;

import builder.LocationSearchBuilder;
import com.google.common.collect.Lists;
import model.*;
import myCache.LocationData;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.List;

public class UserCase03 {

    private static LocationData locationData;
    private static LocationSearchDto locationSearchDtoSaved;

    @BeforeClass
    public static void setUp() {
        locationData = new LocationData();
        locationSearchDtoSaved = LocationSearchBuilder.buildMock(22, 66, 22, 66);
    }

    @Before
    public void setUpMethod() {
        this.locationData.save(locationSearchDtoSaved, Lists.newArrayList(new Coordinate(22, 66)));
    }

    @Test
    public void getDataInCache_listSizeOneTest() {
        assertEquals(this.locationData.getAll().size(), 1);
    }

    @Test
    public void getDataInCache_listSizeEmptyTest() {
        LocationSearchDto dtoEmpty = LocationSearchBuilder.buildMock(23, 41, 50, 60);
        assertTrue(this.locationData.getRoadByCoordinates(dtoEmpty).isEmpty());
    }

    @Test
    public void saveDataInCache_listSizeTwoTest() {
        LocationSearchDto dtoEmpty = LocationSearchBuilder.buildMock(23, 41, 50, 60);
        this.locationData.save(dtoEmpty, Lists.newArrayList(new Coordinate(11, 22)));
        assertEquals(this.locationData.getAll().size(), 2);
    }

    @Test
    public void deleteDataInCache_listSizeEmptyTest() {
        this.locationData.delete(locationSearchDtoSaved);
        assertTrue(this.locationData.getAll().isEmpty());
    }

    @Test
    public void updateDataInCache_ValueUpdatedTest() {
        this.locationData.update(locationSearchDtoSaved, Lists.newArrayList(new Coordinate(99, 77)));
        List<Coordinate> result = this.locationData.getRoadByCoordinates(locationSearchDtoSaved);
        assertEquals(result.get(0), new Coordinate(99, 77));
    }

}