package userstories;

import builder.LocationSearchBuilder;
import implementation.ApiLocationMock;
import static org.junit.Assert.*;
import model.LocationSearchDto;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserCase01 {

    private static ApiLocationMock providerA, providerB, providerC;

    @BeforeClass
    public static void setUp() {
        providerA = new ApiLocationMock();
        providerB = new ApiLocationMock();
        providerC = new ApiLocationMock();

        providerA.off();
        providerB.on();
        providerC.on();
    }

    @Test
    public void providerA_isActiveTest() {
        this.providerA.on();
        assertTrue(this.providerA.isAvailable());
    }

    @Test
    public void providerB_isActiveTest() {
        this.providerB.on();
        assertTrue(this.providerB.isAvailable());
    }

    @Test
    public void providerC_isNotActiveTest() {
        this.providerC.off();
        assertFalse(this.providerC.isAvailable());
    }

    @Test
    public void providerA_isNotActiveTest() {
        this.providerA.off();
        assertFalse(this.providerA.isAvailable());
    }


    @Test
    public void providerB_resultCoordinatesEmptyTest() {
        LocationSearchDto locationSearchDtoEmpty = LocationSearchBuilder.buildMock(23, 41, 50, 60);
        assertTrue(this.providerB.getData(locationSearchDtoEmpty).isEmpty());
    }

    @Test
    public void providerB_resultCoordinatesSizeOneTest() {
        LocationSearchDto locationSearchDtoOne = LocationSearchBuilder.buildMock(22, 66, 22, 66);
        assertEquals(this.providerB.getData(locationSearchDtoOne).size(), 1);
    }

    @Test
    public void providerB_resultCoordinatesSizeThreeTest() {
        LocationSearchDto locationSearchDtoThree = LocationSearchBuilder.buildMock(11, 22, 33, 44);
        assertEquals(this.providerB.getData(locationSearchDtoThree).size(), 3);
    }

}