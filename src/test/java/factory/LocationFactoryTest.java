package factory;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

public class LocationFactoryTest {

    private static MockLocationFactory mockLocationFactory;

    @BeforeClass
    public static void setUp() {
        mockLocationFactory = new MockLocationFactory();
    }

    @Test
    public void testNotNull() {
        assertNotNull(mockLocationFactory.buildCompositeConnector());
    }

}