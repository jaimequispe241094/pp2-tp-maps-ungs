package exceptions;

public class InvalidReadOrWriteFileException extends RuntimeException {

    public InvalidReadOrWriteFileException(String message) {
        super(message);
    }

}