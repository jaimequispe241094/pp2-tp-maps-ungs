package readers;

import exceptions.ConfigurationException;
import model.ConfigurationData;
import model.ConfigurationReader;
import model.ValueDefaultObject;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import java.util.Optional;
import java.util.Properties;

public class PropertyFileReader implements ConfigurationReader {

    private Properties properties;
    private String DEFAULT_VALUE = new ValueDefaultObject().getValue();
    private String filePath;

    public PropertyFileReader(String filePath) {
        this.properties = new Properties();
        this.loadFileProperties(filePath);
        this.filePath = filePath;
    }

    @Override
    public Boolean save(ConfigurationData configurationData) {
        if(StringUtils.isBlank(configurationData.getKey())) {
            throw new ConfigurationException("The key is not valid to save configuration data.");
        }
        try (Writer inputStream = new FileWriter(filePath)) {
            this.properties.setProperty(configurationData.getKey(), configurationData.getData());
            this.properties.store(inputStream, "Database information");
        } catch (IOException ex) {
            System.out.println("Problem occurs when reading file !");
            ex.printStackTrace();
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public Boolean remove(String key) {
        return Optional.ofNullable(this.properties.remove(key)).isPresent();
    }

    @Override
    public ConfigurationData readValue(String key) {
        return new ConfigurationData(key, (String)properties.getOrDefault(key, DEFAULT_VALUE));
    }

    @Override
    public Integer size() {
        return this.properties.size();
    }

    private void loadFileProperties(String pathFile) {
        try {
            this.properties.load(new FileReader(pathFile));
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
            throw new ConfigurationException(pathFile+": file not found");
        } catch (IOException ioe) {
            ioe.printStackTrace();
            throw new ConfigurationException("Error with file - IO");
        }
    }

}