package readers;

import com.google.common.collect.Maps;
import exceptions.InvalidReadOrWriteFileException;
import model.ConfigurationData;
import model.ConfigurationReader;
import java.util.Map;
import java.util.Optional;

public class MockConfigurationReader implements ConfigurationReader {

    private Map<String, String> mockConfiguration;

    public MockConfigurationReader() {
        this.mockConfiguration = Maps.newHashMap();
    }

    @Override
    public Boolean save(ConfigurationData configurationData) {
        if("FILE.OPEN.KEY".equals(configurationData.getKey())) {
            throw new InvalidReadOrWriteFileException("Error - File is open.");
        }
        this.mockConfiguration.put(configurationData.getKey(), configurationData.getData());
        return Boolean.TRUE;
    }

    @Override
    public Boolean remove(String key) {
        return Optional.ofNullable(mockConfiguration.remove(key)).isPresent();
    }

    @Override
    public ConfigurationData readValue(String key) {
        return new ConfigurationData(key, mockConfiguration.getOrDefault(key,""));
    }

    @Override
    public Integer size() {
        return mockConfiguration.size();
    }

}