package main;

import builder.LocationSearchBuilder;
import factory.MockLocationFactory;
import model.ConfigurationData;
import model.LocationProxy;
import model.LocationSearchDto;
import readers.PropertyFileReader;
import services.ServiceProxy;

public class App {

    public static void main(String[] args) {

        LocationProxy service = new ServiceProxy(new MockLocationFactory());
        LocationSearchDto search = LocationSearchBuilder.buildMock(22,66, 22, 66);
        service.getRoad(search).forEach(i->System.out.println(i.getX()+ " - "+ i.getY()));

        PropertyFileReader p = new PropertyFileReader("files/B.properties");
        p.save(new ConfigurationData("asd", "1234"));
    }

}