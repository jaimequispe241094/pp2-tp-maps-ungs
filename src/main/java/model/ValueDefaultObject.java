package model;

import org.apache.commons.lang3.StringUtils;

public class ValueDefaultObject {

    private String value;

    public ValueDefaultObject() {
        this.value = StringUtils.EMPTY;
    }

    public String getValue() {
        return value;
    }

}