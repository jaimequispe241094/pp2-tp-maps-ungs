package model;

public interface ConfigurationReader {

    Boolean save(ConfigurationData configurationData);
    Boolean remove(String key);
    ConfigurationData readValue(String key);
    Integer size();

}