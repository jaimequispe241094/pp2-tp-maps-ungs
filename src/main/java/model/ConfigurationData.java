package model;

public class ConfigurationData {

    private String key;
    private String data;

    public ConfigurationData(String key, String data) {
        this.setKey(key);
        this.setData(data);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}